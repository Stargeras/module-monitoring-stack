module "monitoring" {
  source                 = "./modules/monitoring"
  ingress_domain         = var.ingress_domain
  grafana_admin_password = var.grafana_admin_password
  ingress_class_name     = var.ingress_class_name
  add_loki_datasource    = var.add_loki_datasource
  loki_namespace         = var.loki_namespace
}