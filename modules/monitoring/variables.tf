variable "ingress_domain" {}
variable "grafana_admin_password" {}
variable "loki_namespace" {}
variable "add_loki_datasource" {}
variable "ingress_class_name" {}