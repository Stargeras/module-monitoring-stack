resource "helm_release" "kube-prometheus-stack" {
  name             = "kube-prometheus-stack"
  namespace        = "monitoring"
  create_namespace = "true"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      ingress_domain         = var.ingress_domain
      grafana_admin_password = var.grafana_admin_password
      ingress_class_name     = var.ingress_class_name
      add_loki_datasource    = var.add_loki_datasource
      loki_namespace         = var.loki_namespace
    })
  ]
}
